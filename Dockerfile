FROM python3.7:v1.0

RUN mkdir /usr/local/java
ADD ./jdk-8u333-linux-x64.tar.gz /usr/local/java/
ENV JAVA_HOME /usr/local/java/jdk1.8.0_333
ENV JRE_HOME $JAVA_HOME/jre
ENV CLASSPATH $JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib:$CLASSPATH
ENV PATH $JAVA_HOME/bin:$PATH
ENV PATH /ffmpeg-5.1/:$PATH

RUN mkdir /usr/local/proguard
ADD ./proguard-7.2.2.tar.gz /usr/local/proguard/
ENV PROGUARD_HOME /usr/local/proguard/proguard-7.2.2
ENV PATH $PROGUARD_HOME/bin:$PATH

CMD echo $PATH
CMD echo "success---------ok"

COPY ./requirements.txt /

#RUN pip install --no-cache-dir -r ./requirements.txt
RUN pip install -r ./requirements.txt
RUN pip uninstall -y uvicorn
RUN pip install uvicorn[standard] -i https://mirrors.aliyun.com/pypi/simple

COPY . /project

WORKDIR /project

ENTRYPOINT uvicorn src.main:app --host 0.0.0.0 --port 8000
