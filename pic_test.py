from PIL import Image
from PIL import Image, ImageDraw, ImageFont

image = Image.open("guide.png")
image=image.convert("RGB")
# 定义文字
text = 'hello word'
# 引入PIL库中的模块
draw = ImageDraw.Draw(image)
# 引入字体样式(第一个参数为字体样式，具体的可以在自己电脑中C:\Windows\Fonts下找；第二个参数为字体的大小)
# font = ImageFont.truetype(r'‪C:\Windows\Fonts\simhei.ttf',10)
# 放置到指定位置(第一个100，代表距离图片左边距；第二个100代表距离图片顶部距离；text是定义的文字；fill设置字体颜色值;font是字体引入和大小的变量名称)
draw.text((136, 3195),text , fill='#666666' )
image.save('./out.png')