from Crypto import Random
import base64
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5 as PKCS1_cipher

from src.settings import settings


random_generator = Random.new().read  # 生成随机偏移量
# print(random_generator)
rsa = RSA.generate(1024, random_generator)  # 生成一个私钥
# print(rsa)
# 生成私钥
private_key = rsa.exportKey()  # 导出私钥
# print(private_key.decode())
# 生成公钥
public_key = rsa.publickey().exportKey()  # 生成私钥所对应的公钥
# print(public_key.decode())

with open('rsa_private_key.pem', 'wb')as f:
    f.write(private_key)  # 将私钥内容写入文件中

with open('rsa_public_key.pem', 'wb')as f:
    f.write(public_key)  # 将公钥内容写入文件中

def get_key(key_file):
    with open(key_file) as f:
        data = f.read()  # 获取，密钥信息
        key = RSA.importKey(data)
    return key


def encrypt_data(msg):
    public_key = get_key(f'{settings.RSA_KEY_DIR}/rsa_public_key')  # 读取公钥信息
    cipher = PKCS1_cipher.new(public_key)  # 生成一个加密的类
    encrypt_text = base64.b64encode(cipher.encrypt(msg.encode()))  # 对数据进行加密
    return encrypt_text.decode()  # 对文本进行解码码


def decrypt_data(encrypt_msg):
    private_key = get_key(f'{settings.RSA_KEY_DIR}/rsa_private_key.pem')  # 读取私钥信息
    cipher = PKCS1_cipher.new(private_key)  # 生成一个解密的类
    back_text = cipher.decrypt(base64.b64decode(encrypt_msg), 0)  # 进行解密
    return back_text.decode()  # 对文本内容进行解码
