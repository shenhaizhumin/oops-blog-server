import random
import string
import time
from datetime import datetime

from passlib.context import CryptContext

# 使用的算法是Bcrypt
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def get_password_hash(password: str):
    """
    哈希来自用户的密码
    :param password: 原密码
    :return: 哈希后的密码
    """
    return pwd_context.hash(password.encode())


def verify_password(plain_password, hashed_password):
    """
    校验接收的密码是否与存储的哈希值匹配
    :param plain_password: 原密码
    :param hashed_password: 哈希后的密码
    :return: 返回值为bool类型，校验成功返回True,反之False
    """
    return pwd_context.verify(plain_password, hashed_password)


def get_random_username(count: int = 5):
    # return ''.join(random.sample('abcdefghijklmnopqrstuvwxyz!@#$%^&*()', 5))
    return ''.join(random.sample(string.ascii_letters + string.digits + string.punctuation, count))


def datetime_format(date: datetime):
    return date.strftime('%Y-%m-%d %H:%M:%S')


def time_mills_to_datetime(mills: int):
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(mills // 1000))
