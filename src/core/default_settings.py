#!/usr/bin/python3.6+
# -*- coding:utf-8 -*-
"""
@auth: cml
@date: 2020-9-13
@desc: ...
"""
from pydantic import BaseSettings


class DefaultSetting(BaseSettings):
    class Config:
        case_sensitive = False  # 是否区分大小写

    DEBUG: bool = True
    API_V1_STR: str = "/api/v1"

    DB_URI: str


