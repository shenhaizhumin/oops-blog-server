#!/usr/bin/python3.7+
# -*- coding:utf-8 -*-
"""
@auth: cml
@date: 2020-9-14
@desc: ...

        # 当返回的 Json 串不是一个标准的 Json 时，
        # resp.json() 函数可以传递一个函数对 json 进行预处理，
        # 如：resp.json(replace(a, b))，replace()函数表示 a 替换为 b。

        # aiohttp 使用 response.read() 函数处理字节流，使用 with open() 方式保存文件或者图片
        # response.read() 函数可以传递数字参数用于读取多少个字节，如：response.read(3)读取前 3 个字节。

"""
try:
    import json as _json
except (ImportError, ModuleNotFoundError):
    import json as _json

try:
    import aiohttp
except (ImportError, ModuleNotFoundError):
    pass


class AioHTTPRequest:
    """
    注意：
        不要在接口函数内实例化。应该放在模块层进行实例化，可以供多方调用
    """

    def __init__(self, cookies=None, json_serialize=_json.dumps):
        self.session = aiohttp.ClientSession(
            cookies=cookies,
            json_serialize=json_serialize
        )

    async def get(self, url, params=None, data=None,
                  json=None, headers=None, timeout=30, **kwargs):
        """异步GET请求"""
        return await self.fetch(
            'get', url, params, data, json, headers, timeout, **kwargs)

    async def post(self, url, params=None, data=None,
                   json=None, headers=None, timeout=30, **kwargs):
        """异步POST请求"""
        return await self.fetch(
            'post', url, params, data, json, headers, timeout, **kwargs)

    async def put(self, url, params=None, data=None,
                  json=None, headers=None, timeout=30, **kwargs):
        """异步PUT请求"""
        return await self.fetch(
            'put', url, params, data, json, headers, timeout, **kwargs)

    async def patch(self, url, params=None, data=None,
                    json=None, headers=None, timeout=30, **kwargs):
        """异步PATCH请求"""
        return await self.fetch(
            'patch', url, params, data, json, headers, timeout, **kwargs)

    async def delete(self, url, params=None, data=None,
                     json=None, headers=None, timeout=30, **kwargs):
        """异步DELETE请求"""
        return await self.fetch(
            'delete', url, params, data, json, headers, timeout, **kwargs)

    async def fetch(
            self, method: str, url: str,
            params=None, data=None,
            json=None, headers=None, timeout=30, **kwargs
    ):
        """
        公共请求调用方法

        :param method:  请求方法
        :param url:     请求路由
        :param params:  请求参数
        :param data:    请求的Form表单参数
        :param json:    请求的Json参数
        :param headers: 请求头参数
        :param timeout: 超时时间
        :return:
        """
        __request = getattr(self.session, method.lower())
        async with __request(
                url,
                params=params,
                data=data,
                json=json,
                headers=headers,
                timeout=timeout,
                **kwargs
        ) as response:
            result = await response.json()
        # await self.session.close()
        return result, response.status


def get_request(cookies=None, json_serialize=_json.dumps):
    return AioHTTPRequest(cookies, json_serialize)


async def test_get():
    url = 'http://httpbin.org/get'
    params = {'key1': 'value1', 'key2': 'value2'}
    http_request = get_request()
    resp, status = await http_request.get(url, params)
    assert status == 200
    from pprint import pprint
    pprint(resp)
    return resp


async def test_post():
    url = 'http://httpbin.org/post'
    params = {'key1': 'value1', 'key2': 'value2'}
    http_request = get_request()
    resp, status = await http_request.post(url, params=params, json=params)
    assert status == 200
    from pprint import pprint
    pprint(resp)
    return resp


if __name__ == '__main__':
    import time
    import asyncio

    loop = asyncio.get_event_loop()
    # tasks = [asyncio.create_task(test_post()) for i in range(10)]
    start = time.time()
    # loop.run_until_complete(test_get())
    loop.run_until_complete(test_post())
    # loop.run_forever()
    end = time.time()
    print("花费时间为：")
    print(end - start)

