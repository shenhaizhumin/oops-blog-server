from typing import Optional
from datetime import datetime, timedelta

from fastapi import Depends, HTTPException, status, Request, Header
import jwt

from src.settings import settings, redis_conn
from .response.response import render_error
from .db.db_session import get_db
from src.apps.im.user.models import IMUser


async def create_access_token(data: dict, expires_delta: Optional[int] = None):
    """
    :param data: 需要进行JWT令牌加密的数据（解密的时候会用到）
    :param expires_delta: 令牌有效期
    :return: token
    """
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + + timedelta(seconds=expires_delta)
    else:
        expires_delta = 15 * 60
        expire = datetime.utcnow() + timedelta(seconds=expires_delta)

    # 添加失效时间
    to_encode.update({"exp": expire})
    # SECRET_KEY：密钥
    # ALGORITHM：JWT令牌签名算法
    encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM)
    redis_conn.set(get_token_key(data["id"]), encoded_jwt, ex=expires_delta)
    return encoded_jwt


def get_token_key(uid: str):
    return f'token_{uid}'


async def get_current_user(
        req: Request,
        db=Depends(get_db)
):
    """
    验证token
    # :param db:
    # :param security_scopes:
    :param db:
    :param req:
    :return: 返回用户信息
    """
    path: str = req.get("path")
    if path.startswith("/api/v1/getToken") or path.startswith("/api/v1/register"):
        return
    token = req.headers.get("Authentication")
    return await get_user_by_token(db, token)


async def get_user_by_token(db, token: str):
    if not token:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=render_error("请重新登录", 401),
            # headers={"Authentication": token},
        )
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
        user_id = payload.get("id")
        local_token = redis_conn.get(get_token_key(user_id))
        if local_token != token:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail=render_error("登录已失效，请重新登录", 401),
                headers={"Authentication": token},
            )
        user: IMUser = IMUser.get_user(db, id=user_id)
        if not user:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail=render_error("用户不存在，请重新登录", 401),
                headers={"Authentication": token},
            )
        # 通过解析得到的username,获取用户信息,并返回
        return user.__dict__
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=render_error(str(e), 401),
            headers={"Authentication": token},
        )
