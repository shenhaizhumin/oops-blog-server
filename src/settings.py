import os
import configparser
from pydantic import BaseSettings
import redis

conf = configparser.ConfigParser()
base_path = os.path.dirname(__file__)
profile = os.environ.get('ENV_PROFILE', 'dev')
print(f"当前环境为：{profile}")
if profile == 'production':
    configname = 'config_production.ini'
elif profile == 'testing':
    configname = 'config_testing.ini'
else:
    configname = 'config.ini'
print(f"===>导入的配置文件为：{configname}")
path = os.path.join(base_path, "conf", configname)
print(path)
conf.read(path)


def get_conf_section_dict(
        section: str,
        exclude: set = None,
        conf_parser: configparser.ConfigParser = conf
):
    """
    获取配置文件某个节选的全部数据，转换成字典

    :param section: 节选名称
    :param exclude: 排除的字段
    :param conf_parser: 配置解析器
    :return:
    """
    conf_dict = dict()
    for k in conf_parser.options(section):
        if exclude and k in exclude:
            break
        conf_dict[k] = conf.get(section, k)
    return conf_dict


def mkdir(dir_path):
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)


class Settings(BaseSettings):
    # DB_URI: str = get_conf_section_dict("dsn")["db_uri"]
    DB_URI: str = get_conf_section_dict("postgresql")["db_uri"]
    REDIS_URI: str = get_conf_section_dict("redis")["url"]
    MAPPING_PATH: str = get_conf_section_dict("mapping")["path"]
    LOG_FILES: str = get_conf_section_dict("log_files")["path"]
    AUDIO_FILES: str = get_conf_section_dict("audio_files")["path"]
    RSA_KEY_DIR: str = "rsa_key"
    SECRET_KEY: str = "ce543ba2ee6d61ccfca0158733990c1481cf36b9ce982e2cf813ec0f44ab741f"
    ALGORITHM: str = "HS256"
    ACCESS_TOKEN_EXPIRE_SECONDS: int = 24 * 60 * 60


settings = Settings()
mkdir(settings.MAPPING_PATH)
mkdir(settings.LOG_FILES)
mkdir(settings.AUDIO_FILES)
redis_conn = redis.from_url(settings.REDIS_URI, decode_responses=True)
