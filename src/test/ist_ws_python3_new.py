# -*- coding:utf-8 -*-
#
#   author: iflytek
#
#  本demo测试时运行的环境为：Windows + Python3.6
#  本demo测试成功运行时所安装的第三方库及其版本如下，您可自行逐一或者复制到一个新的txt文件利用pip一次性安装：
#   websocket==0.2.1
#   websocket-client==0.56.0
#
#  语音转写流式 WebAPI 接口参数和错误码 参考接口文档（
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
import websocket
import json
import base64
import hashlib
import math
import uuid
from datetime import datetime
import time
import _thread as thread

STATUS_FIRST_FRAME = 0  # 第一帧的标识
STATUS_CONTINUE_FRAME = 1  # 中间帧标识
STATUS_LAST_FRAME = 2  # 最后一帧的标识


class Ws_Param(object):
    # 初始化
    def __init__(self, URL, APPID, APPKey, AudioFile):
        self.URL = URL
        self.APPID = APPID
        self.APPKey = APPKey
        self.AudioFile = AudioFile

    # 生成url
    def create_header(self):
        appid = self.APPID
        appKey = self.APPKey
        uuid = getUUID()
        # 24 + 32 + 8
        appName = self.URL.split('/')[4]
        for i in range(24 - len(appName)):
            appName += "0"
        capabilityname = appName
        # print(len(capabilityname))
        csid = appid + capabilityname + uuid
        print('CSID...' + csid)
        tmp_xServerParam = {
            "appid": appid,
            "csid": csid
        }

        xCurTime = str(math.floor(time.time()))
        xServerParam = str(base64.b64encode(json.dumps(tmp_xServerParam).encode('utf-8')), encoding="utf8")
        # turn to bytes
        xCheckSum = hashlib.md5(bytes(appKey + xCurTime + xServerParam, encoding="utf8")).hexdigest()

        header = {
            "appKey": appKey,
            "X-Server-Param": xServerParam,
            "X-CurTime": xCurTime,
            "X-CheckSum": xCheckSum
        }

        return header


# 识别结果在此处会实时输出
def on_message(ws, message):
    timeReturn = datetime.now()
    print('on message...')
    get_log('on message...')
    print('voice_convert_time:',
          (timeReturn - ws.timeSend).seconds * 1000 + (timeReturn - ws.timeSend).microseconds / 1000)
    get_log("{}{}".format('voice_convert_time: ',
                          (timeReturn - ws.timeSend).seconds * 1000 + (timeReturn - ws.timeSend).microseconds / 1000))
    get_log_new("{}{}".format('voice_convert_time: ', (timeReturn - ws.timeSend).seconds * 1000 + (
            timeReturn - ws.timeSend).microseconds / 1000))
    print(message)
    get_log(message)
    get_log_new(message)


def on_error(ws, error):
    print(error)
    get_log(str(error))
    get_log_new(str(error))


def on_close(ws):
    print("### closed ###")
    get_log("### closed ###")
    get_log_new("### closed ###")


def on_open(ws):
    def run(*args):
        frameSize = 1280  # 每一帧的音频大小
        intervel = 0.04  # 发送音频间隔(单位:s)
        status = STATUS_FIRST_FRAME  # 音频的状态信息，标识音频是第一帧，还是中间帧、最后一帧

        with open(wsParam.AudioFile, "rb") as fp:
            timeOpen = datetime.now()
            while True:
                buf = fp.read(frameSize)
                # 文件结束
                if not buf:
                    status = STATUS_LAST_FRAME
                # 第一帧处理
                # 发送第一帧音频，带business 参数
                if status == STATUS_FIRST_FRAME:
                    voice_data_bytes = base64.b64encode(buf)
                    # 业务参数,更多参数可在接口查看,参数说明: [endFlag：是否结束 sessionParam会话参数，其中aue音频格式默认pcm，rst返回结果类型 samples 音频字节base64后的字符串]
                    data = {"endFlag": False, "sessionParam": {"aue": "raw", "rst": "plain"},
                            "samples": str(voice_data_bytes, "utf-8")}
                    # print(data)
                    result = json.dumps(data)
                    ws.timeSend = datetime.now()
                    ws.send(result)
                    status = STATUS_CONTINUE_FRAME
                # 中间帧处理
                elif status == STATUS_CONTINUE_FRAME:
                    voice_data_bytes = base64.b64encode(buf)
                    data = {"endFlag": False, "samples": str(voice_data_bytes, "utf-8")}
                    # print(data)
                    result = json.dumps(data)
                    ws.timeSend = datetime.now()
                    ws.send(result)
                # 最后一帧处理
                elif status == STATUS_LAST_FRAME:
                    voice_data_bytes = base64.b64encode(buf)
                    data = {"endFlag": True, "samples": str(voice_data_bytes, "utf-8")}
                    # print(data)
                    result = json.dumps(data)
                    ws.timeSend = datetime.now()
                    ws.send(result)
                    time.sleep(1)
                    break
                # 模拟音频采样间隔
                time.sleep(intervel)

        ws.close()

    thread.start_new_thread(run, ())


def getUUID():
    return "".join(str(uuid.uuid4()).split("-"))


def get_log_new(text):
    with open("/Users/zengqi/PycharmProjects/oops-blog-server/src/test/script/voice.txt", "a") as f:
        f.writelines(text + '\n')


def get_log(text):
    with open("/Users/zengqi/PycharmProjects/oops-blog-server/src/test/logs/aivoice/aivoice.txt", "a") as f:
        f.writelines(text + '\n')


if __name__ == "__main__":
    get_log(time.asctime())
    with open("/Users/zengqi/PycharmProjects/oops-blog-server/src/test/script/voice.txt", "w") as f:
        f.writelines('on message...' + '\n')
    # 测试时候在此处正确填写相关信息即可运行
    time1 = datetime.now()
    # APPID 获取来自授权管理-应用标识 APPKey 获取来自授权管理-应用密钥
    # 语音听写接口鉴权信息
    # wsParam = Ws_Param(URL='ws://172.16.251.142:9070/ws/api/v1/gdist',APPID='gdproxy2', APPKey='5a9aae9f0903fa469b8b8915d681655e',
    #                   AudioFile=r'zhangsan.wav')
    wsParam = Ws_Param(URL='wss://wisdom.it.chinamobile.com:9003/ws/api/v1/gdist', APPID='gdproxy2',
                       APPKey='5a9aae9f0903fa469b8b8915d681655e',
                       # wsParam = Ws_Param(URL='ws://wisdom.it.chinamobile.com:9070/ws/api/v1/gdist',APPID='gdproxy2', APPKey='5a9aae9f0903fa469b8b8915d681655e',
                       AudioFile=r'/Users/zengqi/PycharmProjects/oops-blog-server/src/test/1个小时.wav')
    wsHeader = wsParam.create_header()
    # 开始处理
    ws = websocket.WebSocketApp(wsParam.URL, header=wsHeader, on_message=on_message, on_error=on_error,
                                on_close=on_close)
    import ssl

    # 业务参数参考该方法
    ws.on_open = on_open
    ws.run_forever(sslopt={"cert_reqs": ssl.CERT_NONE})
    time2 = datetime.now()
    print(time2 - time1)
    get_log('{}'.format((time2 - time1).seconds * 1000 + (time2 - time1).microseconds / 1000))
    get_log_new('{}'.format((time2 - time1).seconds * 1000 + (time2 - time1).microseconds / 1000))
