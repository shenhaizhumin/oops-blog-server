import time
from datetime import datetime
import json
import os


# timeStruct1 = datetime.strptime('2022/08/31 03:15:42', "%Y/%m/%d %H:%M:%S")
# timeStruct2 = datetime.strptime('2022/08/31 07:46:27', "%Y/%m/%d %H:%M:%S")
# print((timeStruct2 - timeStruct1).seconds)
def resolution(file_name: str, c_arr: list):
    # file_name = 'gdist-error-0830-11.csv'
    row_data = dict()
    name = file_name.split("/")[1].replace(".csv", ".txt")
    row_data['date'] = '-'.join(file_name.split("/")[1].split('.')[0].split('-')[2:])
    dest_filename = f'new_logs/{name}'
    with open(file_name, mode='r') as f:
        arr = f.read().split("\n")
        # print(arr)
        # pre_ip = ""
        arrays = []
        tmp_arr_ips = set()
        tmp_arr = list()
        for c in arr:
            a = c.split(",")
            tmp_arr.append(a)
            if len(a) == 2:
                tmp_arr_ips.add(a[1])
        for i in tmp_arr_ips:
            tmp = list()
            for j in tmp_arr:
                if len(j) == 2:
                    if i == j[1]:
                        tmp.append(j[0])
            pre_t = None
            l_arr = list()
            t_arr = list()
            data = dict(ip=i, count=0, times=t_arr)
            # if i=='117.136.52.240':
            #     print(i)
            next_index = None
            for t in tmp:
                # for t in tmp:
                if not pre_t:
                    pre_t = t

                a = datetime.strptime(t, "%Y/%m/%d %H:%M:%S")
                b = datetime.strptime(pre_t, "%Y/%m/%d %H:%M:%S")
                s = (a - b).seconds
                if s <= 60 * 5:
                    data['count'] += 1
                    t_arr.append(t)
                else:
                    pre_t = t
                    if data['count'] >= 6:
                        l_arr.append(data)
                        arrays.append(data)
                    # else:
                    #     next_index
                    t_arr = []
                    data = dict(ip=i, count=0, times=t_arr)
            if data['count'] >= 6:
                l_arr.append(data)
                arrays.append(data)

            # if l_arr:
            #     arrays.append(l_arr)
        # 天数 次数 人数
        content = f"记录条目数：{len(arrays)}\n"
        # print(f"记录条目数：{len(arrays)}")
        # print(json.dumps(arrays))
        tmp_arr = set()
        result = list()
        # total_count = len(arrays)  # 总次数
        row_data['total_count'] = len(arrays)
        for a in arrays:
            # print(json.dumps(a))
            content += f'{json.dumps(a)}\n'
            if not tmp_arr.__contains__(a['ip']):
                tmp_arr.add(a['ip'])
                result.append(a)
        # print(content)
        # print(f"去重后记录条目数：{len(result)}")
        # user_count = len(result)  # 总人数
        row_data['user_count'] = len(result)
        c_arr.append(row_data)
        content += f"去重后记录条目数：{len(result)}\n"
        for r in result:
            # print(json.dumps(r))
            content += f'{json.dumps(r)}\n'
        print(content)
        with open(dest_filename, mode='wb') as file:
            file.write(content.encode())


def merge():
    r = 'gdist'
    new = 'gdist_new'
    names = dict()
    for name in os.listdir(r):
        d = name.split('.')[0].split('-')[2]
        value = names.get(d)
        if not value:
            l = [name]
            names[d] = l
        else:
            names[d].append(name)
    for k, v in names.items():
        filename = f'{new}/gdist-error-{k}.csv'
        content = ''
        for i in v:
            with open(f'gdist/{i}', 'r') as f:
                content += f.read()
        with open(filename, mode='wb') as f:
            f.write(content.encode())


if __name__ == '__main__':
    # merge()
    import os

    root_dir = 'gdist_new'
    arr = []
    for filename in os.listdir(root_dir):
        resolution(f'{root_dir}/{filename}', arr)
    c = ''
    for i in arr:
        c += f'{json.dumps(i)}\n'
    with open('new_logs/result.txt', mode='wb') as f:
        f.write(c.encode())
    # arr_x = dict()
    # for i in arr:
    #     date = i['date'].split('-')[0]
    #     v = arr_x.get(date)
    #     if not v:
    #         arr_x[date] = dict(date=date, total_count=i['total_count'], user_count=i['user_count'])
    #     else:
    #         v['total_count'] += i['total_count']
    #         v['user_count'] += i['user_count']
    #     # c += f'{json.dumps(i)}\n'
    # # print(arr_x)
    # for i in arr_x.values():
    #     c += f'{json.dumps(i)}\n'
    # with open('new_logs/result2.txt', mode='wb') as f:
    #     f.write(c.encode())

    """
echo "deb http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-security main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-updates main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-proposed main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ bionic-backports main restricted universe multiverse" >> /etc/apt/sources.list
     
id->auto increament primary key 自增长主键
 
uid->integer/varchar 该条消息所属消息，比如我登陆了，我发送/接收到消息入库的时候写入自己的uid，他的作用是多用户登陆的时候区分回话表
 
chatId->integet/varchar 服务器生产回话 id当前的回话id，它作用是标识一个回话，比如我跟你聊天or 你跟我聊天，我们的回话id应该是一致的，对于群聊也是，在群中发送消息，每个人的回话id是一致的。
 
c_id->varchar unique，他是标记一台设备上某个用户的唯一回话，用于更新避免插入多条数据的，可以用到sqlite的update or replace，他的值可以是hash(uid+chatid)或者其他，该字段可以只是客户端具有，客户端生成并且自己维护。
 
from->integer/varchar 发送人id(自己发送就是自己的uid，不然就是别人的uid)
 
to->integer/varchar 接收人id(uid/group_id)
 
last_msg->varchar 最后的一条消息内容
last_msg_id->integer/varchar  最后一条消息的id，作用是用于比较，比如在聊天页面中，删除了一条消息，撤回了一条消息之类的，这时候可以根据这个msg_id进行删除修改和更新。
chat_name -> varchar 聊天者的名称，比如我与你聊天，我的表中的这个字段就是你的name,你的表中就是我的name.
 
last_user_name->varchar 最后的发送者名称
 
last_time->integer 最后消息发送时间
 
chat_type->integer 回话类型(群组消息/个人聊天/系统消息)
 
msg_type->integer 消息类型(文字/图片/文件/音乐等)
 
unread_count->integer 改回话未读数目
 
复制代码
    """
