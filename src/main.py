import time

from fastapi import FastAPI, Depends, Request
from fastapi.exceptions import HTTPException, ValidationError, RequestValidationError, FastAPIError
from fastapi.responses import JSONResponse

from src.apps.parse.views import parse_router
from src.core.jwt_token import get_current_user
from src.apps.article.views import article_router
from src.apps.im.user.views import user_router
from src.apps.im.chat.views import ws_router
from src.apps.tools.views import tools_router
from src.apps.asr.views import router_asr, router_asr_experience

"""
docker run -p 6379:6379 --name redis -v ~/redis/redis.conf:/etc/redis/redis.conf -v ~/redis/data:/data -d redis redis-server /etc/redis/redis.conf 

"""
# app = FastAPI(title="我的博客", dependencies=[Depends(get_current_user)])
app = FastAPI(title="我的博客")
api_prefix = "/api/v1"
app.include_router(article_router, prefix=api_prefix, tags=["文章接口"])
app.include_router(user_router, prefix=api_prefix, tags=["用户接口"])
app.include_router(ws_router, prefix=api_prefix, tags=["聊天接口"])
app.include_router(tools_router, prefix=f'{api_prefix}/tools', tags=["工具接口"])

# app.include_router(router_asr, prefix=f'{api_prefix}/asr', tags=["语音识别"])
# app.include_router(router_asr_experience, prefix=f'{api_prefix}/asr', tags=["语音识别体验"])
app.include_router(parse_router, prefix=f'{api_prefix}/parse', tags=["解析url接口"])


# Press the green button in the gutter to run the script.
# if __name__ == '__main__':
#     print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/

# @app.middleware("http")
# async def add_process_time_header(request: Request, call_next):
#     start_time = time.time()
#     body = await request.body()
#
#     print("body:", body.decode())
#     print("req:", await request.json())
#     response = await call_next(request)
#     process_time = time.time() - start_time
#     response.headers["X-Process-Time"] = str(process_time)
#     return response

@app.exception_handler(HTTPException)
async def http_exception_v1(request: Request, exc: HTTPException):
    if type(exc.detail) == dict:
        return JSONResponse(dict(code=exc.status_code, message=str(exc.detail["message"])), status_code=exc.status_code)
    else:
        return JSONResponse(dict(code=exc.status_code, message=str(exc.detail)), status_code=exc.status_code)


@app.exception_handler(RequestValidationError)
async def http_exception_v2(request: Request, exc: RequestValidationError):
    return JSONResponse(dict(code=400, message=str(exc.errors())), status_code=400)


@app.exception_handler(FastAPIError)
async def http_exception_v3(request: Request, exc: FastAPIError):
    return JSONResponse(dict(code=500, message=str(exc)), status_code=500)

"""
ffmpeg -i "concat:1.wav|2.wav" output.wav
"""
if __name__ == '__main__':
    import uvicorn

    uvicorn.run(app, host='0.0.0.0')
    # config = uvicorn.Config(app)
    # uvicorn.Server(config).run()
