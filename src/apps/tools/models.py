from sqlalchemy import String, Integer, Column, DateTime
from src.core.db.db_session import Base, engine
from datetime import datetime


class ProguardMapping(Base):
    __tablename__ = "proguard_mapping"

    id = Column(Integer, primary_key=True)
    app_version = Column(String(100), nullable=False)
    mapping_path = Column(String(100), nullable=False)
    create_time = Column('f_create_time', DateTime, default=datetime.now)
    update_time = Column('f_update_time', DateTime, default=datetime.now, onupdate=datetime.now)


Base.metadata.create_all(bind=engine)
