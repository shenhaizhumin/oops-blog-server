from .models import ProguardMapping


class ToolsController:
    @classmethod
    def get_mapping_by_app_version(cls, db, app_version):
        return db.query(ProguardMapping).filter(ProguardMapping.app_version == app_version).first()

    @classmethod
    def save_mapping(cls, db, app_version, file_path):
        db.add(ProguardMapping(app_version=app_version, mapping_path=file_path))
        db.commit()

    @classmethod
    def update_mapping(cls, db, app_version, file_path):
        mapping = cls.get_mapping_by_app_version(db, app_version)
        mapping.mapping_path = file_path
        db.commit()
