import os
import time
from datetime import datetime

from fastapi import APIRouter, UploadFile, File, Form, Depends, HTTPException

from src.core.db.db_session import get_db
from src.core.response.response import render_resp
from src.settings import settings
from .models import ProguardMapping
from .controllers import ToolsController

tools_router = APIRouter()


@tools_router.post("/uploadMappingFile", description="上传打包混淆时生成的mapping.txt")
async def upload_mapping(file: UploadFile = File(..., description="mapping文件"),
                         app_version: str = Form(..., alias="appVersion", description="app版本号"),
                         db=Depends(get_db)):
    content = await file.read()
    file_path = f"{settings.MAPPING_PATH}/{time.time()}-{file.filename}"
    with open(file_path, mode='wb') as f:
        f.write(content)
        print(f"mapping文件已被写入至{file_path}")
    proguard_mapping = ToolsController.get_mapping_by_app_version(db, app_version)
    if proguard_mapping:
        # 该版本的mapping文件已上传过，覆盖
        ToolsController.update_mapping(db, app_version, file_path)
        return render_resp(dict(file_path=file_path, app_version=app_version))
    ToolsController.save_mapping(db, app_version, file_path)
    print("mapping配置记录成功")
    return render_resp(dict(file_path=file_path, app_version=app_version))


@tools_router.post("/convertLogFile", description="混淆的日志转换为开发日志")
async def convert_log(file: UploadFile = File(..., description="错误日志文件"),
                      mapping_file: UploadFile = File(None, description="mapping文件"),
                      app_version: str = Form(None, alias="appVersion", description="版本号"),
                      db=Depends(get_db)):
    if not mapping_file and not app_version:
        raise HTTPException(status_code=400, detail="mapping_file/app_version必须上传其中一个")
    # java -jar retrace.jar ./mapping.txt ./error.txt
    error_log = await file.read()
    log_file = f'{settings.LOG_FILES}/{time.time()}'
    with open(log_file, mode='wb') as log_f:
        log_f.write(error_log)
        print(f"错误日志已保存到本地:{log_file}")
    if mapping_file:
        mapping_content = await mapping_file.read()
        mapping_file = f"{settings.MAPPING_PATH}/{time.time()}-{mapping_file.filename}"
        with open(mapping_file, mode='wb') as mapping_file:
            mapping_file.write(mapping_content)
        cmd = f'retrace.sh {mapping_file} {log_file}'
        resp = os.popen(cmd).read()
        print(f"解码混淆日志响应 :{resp}")
        return render_resp(resp)
    else:
        proguard_mapping = ToolsController.get_mapping_by_app_version(db, app_version)
        if proguard_mapping:
            cmd = f'retrace.sh {proguard_mapping.mapping_path} {log_file}'
            print(f"cmd:{cmd}")
            resp = os.popen(cmd).read()
            print(f"解码混淆日志响应 :{resp}")
            return render_resp(resp)
        else:
            return render_resp(message=f"找不到{app_version} 对应的mapping文件")
