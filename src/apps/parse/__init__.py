import base64

LOCAL_PROXY_HOST = "127.0.0.1"
SEG_PROXY_SPLIT_STR = "&jeffmony_seg&"
VIDEO_PROXY_SPLIT_STR = "&jeffmony_video&"

if __name__ == '__main__':
    url = 'http://127.0.0.1:40211/aHR0cHM6Ly91cC5qaWxpZ3VhbGEuY29tL2NvdXJzZS12aWRlb3MvbGNkcy9MQ0RTMDg0My0yMDE5MTEyNzA5MTQxNjI4NS5tcDQmamVmZm1vbnlfdmlkZW8mbm9uX20zdTgmamVmZm1vbnlfdmlkZW8mdW5rbm93bg'
    if url and LOCAL_PROXY_HOST in url:
        index = url.rfind("/")
        encode = url[index + 1:]
        print(encode)
        missing_padding = 4 - len(encode) % 4
        if missing_padding:
            encode += '=' * missing_padding
        decode_str = base64.b64decode(encode).decode()
        print(decode_str)
        if VIDEO_PROXY_SPLIT_STR in decode_str:
            str_arr = decode_str.split(VIDEO_PROXY_SPLIT_STR)
            res = decode_str
            if len(str_arr) == 3:
                res = str_arr[0]
            print(res)
        elif SEG_PROXY_SPLIT_STR in decode_str:
            str_arr = decode_str.split(SEG_PROXY_SPLIT_STR)
            res = decode_str
            if len(str_arr) == 3:
                res = str_arr[0]
            print(res)
