from fastapi import APIRouter, Form
import base64

parse_router = APIRouter()
from src.core.response.response import render_resp

LOCAL_PROXY_HOST = "127.0.0.1"
SEG_PROXY_SPLIT_STR = "&jeffmony_seg&"
VIDEO_PROXY_SPLIT_STR = "&jeffmony_video&"


@parse_router.post("/url", description="需要解析的url")
async def parse_url(url: str = Form(...)):
    if url and LOCAL_PROXY_HOST in url:
        index = url.rfind("/")
        encode = url[index + 1:]
        print(encode)
        missing_padding = 4 - len(encode) % 4
        if missing_padding:
            encode += '=' * missing_padding
        decode_str = base64.b64decode(encode).decode()
        print(decode_str)
        res = decode_str
        if VIDEO_PROXY_SPLIT_STR in decode_str:
            str_arr = decode_str.split(VIDEO_PROXY_SPLIT_STR)
            if len(str_arr) == 3:
                res = str_arr[0]
            print(res)
        elif SEG_PROXY_SPLIT_STR in decode_str:
            str_arr = decode_str.split(SEG_PROXY_SPLIT_STR)
            if len(str_arr) == 3:
                res = str_arr[0]
            print(res)
        return render_resp(res)
    else:
        return render_resp(message=f"解析不到{url} 对应的播放链接")
