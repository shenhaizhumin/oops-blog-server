from fastapi import APIRouter
from src.core.response.response import render_resp
from src.core.logger import get_logger

article_router = APIRouter()
logger = get_logger("article")


@article_router.get("/blog", description="获取文章详情")
async def get_user():
    logger.info("article info")
    return render_resp(dict(name='article'))
