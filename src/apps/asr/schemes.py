from pydantic import BaseModel, Field
from .modles import Language


class RecordScheme(BaseModel):
    file_id: str = Field(None, description="音频文件路径")
    name: str = Field(..., description='用户定义的记录名称')
    app_file_id: str = Field(..., description="客户端的音频文件名称")
    lang: str = Field(Language.CN)


class FileScheme(BaseModel):
    file_id: str = Field(..., description="音频文件路径")
    record_id: str = Field(..., description="记录id")
    lang: str = Field(Language.CN)
