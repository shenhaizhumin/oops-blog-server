import os
import shutil

import auditok
import librosa
from paddlespeech.cli.asr.infer import ASRExecutor
from paddlespeech.cli.text.infer import TextExecutor
from fastapi import HTTPException
from paddlespeech.s2t.utils.log import Log

from src.settings import settings
from .modles import Record, RecognitionState, Language
from src.core.response.response import render_error


class AsrController:
    def __init__(self):
        self.logger = Log(__name__).getlog()
        self.min_dur = 1
        self.max_dur = 50
        self.max_silence = 1
        self.menergy_threshold = 55
        self.sample_rate = [16000, 8000]
        # self.asr = ASRExecutor()

    def create_record(self, db, record_name, app_file_id, file_id, state=RecognitionState.STATE_NORMAL):
        record = Record(app_file_id=app_file_id, file_id=file_id, name=record_name, state=state)
        db.add(record)
        db.commit()
        db.refresh(record)
        return record

    def update_record(self, db, record_id, name, file_id, lang, state, background_tasks):
        record = self.get_record(db, id=record_id)
        if not record:
            raise HTTPException(400, detail=render_error(f"找不到id为{record_id}的记录"))
        if name:
            record.name = name
        if file_id and state == RecognitionState.STATE_CONVERTING:
            # 音频文件路径
            record.file_id = file_id
            if record.state == RecognitionState.STATE_NORMAL:
                background_tasks.add_task(self.audio_file_to_text, db, file_id, record_id, lang)
                record.state = state
                self.logger.info("转写任务已添加到后台")
            else:
                raise HTTPException(400, detail=render_error(f"该记录正在转换中..."))
        db.commit()

    def get_record_list(self, db):
        return db.query(Record).all()

    def get_record(self, db, **kwargs):
        return db.query(Record).filter_by(**kwargs).first()

    def demo_reco_small_file(self, audio_file, lang=Language.CN):
        try:
            return _ctrl_punc.process_text(self.reco(audio_file, lang))
        except Exception as e:
            return str(e)

    def reco(self, audio_file, lang=Language.CN):
        # 调用paddlespeech语音识别功能
        asr = ASRExecutor()
        if lang == Language.CN:
            # 中文转写
            result = asr(audio_file=audio_file)
        else:
            # 英文转写
            result = asr(audio_file=audio_file, lang=lang, model="transformer_librispeech")
        self.logger.info(f'result:{result}')
        return result

    def get_audio_sample_rate(self, audio_file):
        y, sr = librosa.load(audio_file, sr=None)
        return sr

    def save_audio_file(self, audio_data, filename: str):
        audio_file = f'{settings.AUDIO_FILES}/{filename}'
        with open(audio_file, mode='wb') as f:
            f.write(audio_data)
        if not filename.endswith(".wav") or self.get_audio_sample_rate(audio_file) not in self.sample_rate:
            return self.audio_file_to_wav(audio_file)
        return audio_file

    def audio_file_to_wav(self, audio_file):
        """任意格式转换成 采样率16k 单声道的wav音频"""
        # os.popen(f'sox {audio_file} --rate 16k --bits 16 --channels 1 {audio_file}.wav')
        cmd = f'ffmpeg -i {audio_file} -ar 16000 {audio_file}.wav'
        os.popen(cmd)
        self.logger.info(f"{audio_file} ---> {audio_file}.wav")
        exists = os.path.exists(f'{audio_file}.wav')
        self.logger.info(f'{audio_file}.wav exists {exists}')
        return f'{audio_file}.wav'

    def audio_file_to_text(self, db, file, record_id, lang):
        result, reason = self.reco_big_file(file, lang)
        self.logger.info(f"{result} reason:{reason}")
        # 修改记录状态
        record = self.get_record(db, id=record_id)
        if record:
            if result:
                record.state = RecognitionState.STATE_SUCCESS
                record.content = result
            else:
                record.state = RecognitionState.STATE_FAILED
                record.reason = reason if reason else "文件为空白音频"
            db.commit()
        self.logger.info(f'{file} 转写完成！')

    def reco_big_file(self, audio_file, lang):
        if not os.path.exists(audio_file):
            self.logger.info(f"{audio_file}音频文件不存在...")
            return None, None
        filename = audio_file.split('/')[-1].split(".")[0]
        self.logger.info(f"{audio_file} 转写任务开始...")
        audio_cut_dir = f'{settings.AUDIO_FILES}/tmp_cut_{filename}'
        if not os.path.exists(audio_cut_dir):
            os.makedirs(audio_cut_dir)
        try:
            # 音频智能分片段
            audio_regions = auditok.split(
                audio_file,
                min_dur=self.min_dur,  # minimum duration of a valid audio event in seconds
                max_dur=self.max_dur,  # maximum duration of an event
                # maximum duration of tolerated continuous silence within an event
                max_silence=self.max_silence,
                energy_threshold=self.menergy_threshold  # threshold of detection
            )
            result = ''
            for r in audio_regions:
                audio_cut = f'{audio_cut_dir}/{round(r.meta.start, 3)}-{round(r.meta.end, 3)}.wav'
                r.save(audio_cut)
                self.logger.info(f'{audio_cut} 正在转写!')
                result = ''.join([result, self.reco(audio_cut, lang)])
                self.logger.info(f'curResult:', result)
                self.logger.info(f'{audio_cut} 转写完毕!')
            # 删除剪切的音频
            shutil.rmtree(audio_cut_dir)
            # os.remove(audio_file)
            if result:
                return _ctrl_punc.process_text(result), None
            else:
                return result, None
        except Exception as e:
            # 删除剪切的音频
            shutil.rmtree(audio_cut_dir)
            # os.remove(audio_file)
            return '', str(e)

    def demo_convert_file_to_text(self, audio_file, lang=Language.CN):
        if not os.path.exists(audio_file):
            self.logger.info(f"{audio_file}音频文件不存在...")
            return None, None
        filename = audio_file.split('/')[-1].split(".")[0]
        self.logger.info(f"{audio_file} 转写任务开始...")
        audio_cut_dir = f'{settings.AUDIO_FILES}/tmp_cut_{filename}'
        if not os.path.exists(audio_cut_dir):
            os.makedirs(audio_cut_dir)
        try:
            # 音频智能分片段
            audio_regions = auditok.split(
                audio_file,
                # min_dur=0.2,  # minimum duration of a valid audio event in seconds
                min_dur=self.min_dur,  # minimum duration of a valid audio event in seconds
                # max_dur=5,  # maximum duration of an event
                max_dur=self.max_dur,  # maximum duration of an event
                # maximum duration of tolerated continuous silence within an event
                max_silence=self.max_silence,
                energy_threshold=self.menergy_threshold  # threshold of detection
            )
            # result = list()
            result = ''
            for r in audio_regions:
                audio_cut = f'{audio_cut_dir}/{round(r.meta.start, 3)}-{round(r.meta.end, 3)}.wav'
                r.save(audio_cut)
                self.logger.info(f'{audio_cut} 正在转写!')
                # result.append(dict(
                #     start=int(r.meta.start * 1000),
                #     end=int(r.meta.end * 1000),
                #     # content=_ctrl_punc.process_text(self.reco(audio_cut, lang)),
                #     content=_ctrl_punc.process_text(self.reco(audio_cut, lang)),
                # ))
                result = ''.join([result, _ctrl_punc.process_text(self.reco(audio_cut, lang))])
                self.logger.info(f'curResult:', result)
                self.logger.info(f'{audio_cut} 转写完毕!')
            # 删除剪切的音频
            shutil.rmtree(audio_cut_dir)
            # os.remove(audio_file)
            if result:
                return result, None
            else:
                return None, 'empty file'
        except Exception as e:
            # 删除剪切的音频
            shutil.rmtree(audio_cut_dir)
            # os.remove(audio_file)
            return None, str(e)


class PuncController:

    def __init__(self):
        # Punctuation Restoration 文本加标点
        self.text_punc = TextExecutor()

    def process_text(self, text):
        return self.text_punc(text=text)


ctrl_asr = AsrController()
_ctrl_punc = PuncController()
