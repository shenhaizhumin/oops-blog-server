import os.path

from fastapi import APIRouter, File, UploadFile, Depends, BackgroundTasks, HTTPException, Path, Query

from .controllers import ctrl_asr
from src.core.response.response import render_resp, render_error
from src.core.db.db_session import get_db
from .modles import RecognitionState, Language
from . import schemes

router_asr = APIRouter()
router_asr_experience = APIRouter()


@router_asr.get("/getRecordList")
async def get_record_list(db=Depends(get_db)):
    return render_resp(ctrl_asr.get_record_list(db))


@router_asr.post("/uploadFile", description="上传音频文件")
async def upload(file: UploadFile = File(...)):
    audio_file = ctrl_asr.save_audio_file(await file.read(), file.filename)
    return render_resp(dict(file_id=audio_file))


@router_asr.post("/createRecord")
async def create_record(background_tasks: BackgroundTasks, scheme: schemes.RecordScheme, db=Depends(get_db)):
    if scheme.lang not in [Language.CN, Language.EN]:
        raise HTTPException(status_code=400, detail=render_error(f"语言可选项为{Language.CN}-->中文{Language.EN}-->英文"))
    if scheme.file_id and os.path.exists(scheme.file_id):
        record = ctrl_asr.create_record(
            db,
            scheme.name,
            scheme.app_file_id,
            scheme.file_id,
            RecognitionState.STATE_CONVERTING
        )
        background_tasks.add_task(ctrl_asr.audio_file_to_text, db, scheme.file_id, record.id, scheme.lang)
        print("转写任务已添加到后台")
    else:
        record = ctrl_asr.create_record(db, scheme.name, scheme.app_file_id, scheme.file_id)
    return render_resp(record)


@router_asr.put("/addTask", description="开始转换")
async def add_task(background_tasks: BackgroundTasks,
                   scheme: schemes.FileScheme,
                   db=Depends(get_db)):
    if scheme.lang not in [Language.CN, Language.EN]:
        raise HTTPException(status_code=400, detail=render_error(f"语言可选项为{Language.CN}-->中文{Language.EN}-->英文"))
    ctrl_asr.update_record(
        db=db,
        record_id=scheme.record_id,
        file_id=scheme.file_id,
        state=RecognitionState.STATE_CONVERTING,
        lang=scheme.lang,
        background_tasks=background_tasks
    )
    return render_resp('ok')


@router_asr.get("/getRecordDetail/{id}", description="获取记录详情")
async def get_record_detail(record_id: int = Path(..., alias="id", description="记录id"), db=Depends(get_db)):
    record = ctrl_asr.get_record(db, id=record_id)
    if not record:
        raise HTTPException(status_code=404, detail=render_error(f"找不到id为{record_id}的记录"))
    return render_resp(record)


@router_asr_experience.post("/recognition", description="识别文件，可传递大文件")
async def reco(lang: str = Query(Language.CN, description=f"语言可选项为{Language.CN}-->中文{Language.EN}-->英文"),
               file: UploadFile = File(...)):
    if lang not in [Language.CN, Language.EN]:
        raise HTTPException(status_code=400, detail=render_error(f"语言可选项为{Language.CN}-->中文{Language.EN}-->英文"))
    audio_data = await file.read()
    file = ctrl_asr.save_audio_file(audio_data, file.filename)
    result, error = ctrl_asr.demo_convert_file_to_text(file, lang)
    if result:
        return render_resp(data=result)
    else:
        return render_error(error)


@router_asr_experience.post("/recognition/smallFile", description="识别小文件")
async def reco_small_file(lang: str = Query(Language.CN, description=f"语言可选项为{Language.CN}-->中文{Language.EN}-->英文"),
                          file: UploadFile = File(...)):
    if lang not in [Language.CN, Language.EN]:
        raise HTTPException(status_code=400, detail=render_error(f"语言可选项为{Language.CN}-->中文{Language.EN}-->英文"))
    audio_data = await file.read()
    file = ctrl_asr.save_audio_file(audio_data, file.filename)
    result = ctrl_asr.demo_reco_small_file(file, lang)
    return render_resp(data=result)
