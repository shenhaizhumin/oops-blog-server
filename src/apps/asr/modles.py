from datetime import datetime
import uuid

from sqlalchemy import String, Integer, Column, DateTime, SmallInteger, ForeignKey, or_
from sqlalchemy.orm import relationship

from src.core.db.db_session import Base, engine


class RecognitionState:
    STATE_NORMAL = 0  # 初始化
    STATE_CONVERTING = 1  # 转换中
    STATE_FAILED = 2  # 识别失败
    STATE_SUCCESS = 3  # 识别成功


class Language:
    CN = "zh"
    EN = "en"


class Record(Base):
    __tablename__ = 'record'

    id = Column(Integer, primary_key=True)
    uid = Column(String, index=True, nullable=False, default=str(uuid.uuid4()).replace('-', ''))
    app_file_id = Column(String, comment="音频文件名称")
    file_id = Column(String, comment='音频文件位置')
    name = Column(String, comment='用户自定义的记录名称')
    content = Column(String)
    reason = Column(String, comment="转写失败的原因")
    language = Column(String, default=Language.CN)
    state = Column(SmallInteger, default=RecognitionState.STATE_NORMAL)
    create_time = Column('create_time', DateTime, default=datetime.now)
    update_time = Column('update_time', DateTime, default=datetime.now, onupdate=datetime.now)


Base.metadata.create_all(bind=engine)
