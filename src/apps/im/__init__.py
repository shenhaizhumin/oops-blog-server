class MessageType:
    # 消息类型(文字/图片/文件/音乐等)
    TEXT = 0
    PIC = 1
    FILE = 2
    MUSIC = 3


# 会话类型(群组消息/个人聊天/系统消息)
class SessionType:
    GROUP = 0
    ONE_TO_ONE = 1
    SYSTEM = 2


class MessageStatus:
    RECEIVED = 1
    UNRECEIVED = 0
