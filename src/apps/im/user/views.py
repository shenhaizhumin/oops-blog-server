from fastapi import APIRouter, Depends, Form, Security, Path, Query, HTTPException

from src.core.db.db_session import get_db
from src.core.response.response import render_resp, render_list_resp, render_error
from src.core.logger import get_logger
from .controllers import user_ctrl
from . import schemes
from src.core.jwt_token import get_current_user

user_router = APIRouter()
logger = get_logger("user")


@user_router.get("/contact/search")
async def search_contact(
        keyword: str = Query(None, description="关键字"),
        # mobile: str = Query(None, description="手机号"),
        # email: str = Query(None, description="邮箱"),
        user_data=Depends(get_current_user),
        db=Depends(get_db)
):
    # if not any([nickname, mobile, email]):
    #     raise HTTPException(status_code=400, detail=render_error("请输出昵称/邮箱/手机号"))
    if not keyword:
        raise HTTPException(status_code=400, detail=render_error("请输入关键字"))
    users = user_ctrl.search_contact(db, user_data['id'], keyword)
    return render_list_resp([schemes.UserModelScheme.from_orm(u) for u in users])


@user_router.get("/contacts", description="获取用户联系人")
async def get_contacts(
        db=Depends(get_db),
        user_data=Depends(get_current_user)
):
    contacts = user_ctrl.get_contacts(db, user_data['id'])
    if not contacts:
        return render_list_resp()
    return render_list_resp([schemes.UserModelScheme.from_orm(u) for u in contacts])


@user_router.put("/contact/{friendId}", description="添加联系人")
async def add_contact(
        friend_id: int = Path(..., alias="friendId", description="联系人id"),
        db=Depends(get_db),
        user_data=Depends(get_current_user)
):
    await user_ctrl.add_contact(db, user_data['id'], friend_id)
    return render_resp("ok")


@user_router.post("/contact/agree/{id}", description="同意好友申请")
async def agree(msg_id: int = Path(..., alias="id", description="消息id"),
                db=Depends(get_db),
                user_data=Depends(get_current_user)):
    await user_ctrl.agree_friend(db, msg_id, user_data['id'])
    return render_resp('ok')


@user_router.get("/contact/getFriendMsg", description="获取好友申请消息")
async def get_friend_msg(db=Depends(get_db),
                         user_data=Depends(get_current_user)):
    return render_list_resp(user_ctrl.get_friend_msg(db, user_data['id']))


@user_router.delete("/contact/{friendId}", description="删除联系人")
async def delete_contact(
        friend_id: int = Path(..., alias="friendId", description="联系人id"),
        db=Depends(get_db),
        user_data=Depends(get_current_user)
):
    user_ctrl.delete_contact(db, user_data['id'], friend_id)
    return render_resp("ok")


@user_router.get("/user", description="获取用户")
async def get_user(
        db=Depends(get_db),
        user_data=Depends(get_current_user)
):
    logger.info(f"user info:{user_data}")
    user_info = user_ctrl.get_user(db, id=user_data['id'])
    return render_resp(schemes.UserModelScheme.from_orm(user_info))


@user_router.post("/register")
async def register(user: schemes.UserScheme, db=Depends(get_db)):
    return render_resp(schemes.UserModelScheme.from_orm(user_ctrl.register(db, **user.dict())))


@user_router.post("/getToken")
async def login(account: str = Form(...), passwd: str = Form(...), db=Depends(get_db)):
    return render_resp(await user_ctrl.create_token(db, account, passwd))
