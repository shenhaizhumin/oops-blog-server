import datetime
import hashlib

from sqlalchemy.orm.session import Session
from fastapi import HTTPException
from sqlalchemy import or_, not_, and_

from src.core.response.response import render_resp, render_error
from .models import IMUser, IMUserContact, IMAddFriendMessage
from src.core.jwt_token import create_access_token
from src.settings import settings
from src.apps.im.chat.controllers import ws_ctrl
from src.utils import datetime_format


class UserController:
    def __init__(self):
        pass

    def search_contact(self, db, user_id, keyword):
        user_friend = IMUserContact.get_user_contact(db, user_id)
        friends = user_friend.friends if user_friend else [user_id]
        return db.query(IMUser).filter(
            IMUser.id.notin_(friends),
            or_(
                IMUser.nickname.like(f"%{keyword}%"),
                IMUser.mobile.like(f"%{keyword}%"),
                IMUser.email.like(f"%{keyword}%")
            )
        ).all()

    def create_user_friend_ship(self, db, user_id, friend_id):
        friends = [friend_id]
        db.add(IMUserContact(user_id=user_id, friends=friends))
        db.commit()

    async def add_contact(self, db, user_id, friend_id):
        # user_friend = IMUserFriend.get_user_friend(db, user_id)
        # if not user_friend:
        #     self.create_user_friend_ship(db, user_id, friend_id)
        #     return True
        # user_friend.friends.append(friend_id)

        if IMAddFriendMessage.get(db, from_user_id=user_id, to_user_id=friend_id):
            raise HTTPException(status_code=400, detail=render_error("消息已发送"))
        sent_time = datetime.datetime.now()
        msg = IMAddFriendMessage.create(db, from_user_id=user_id, to_user_id=friend_id,
                                        sent_time=sent_time)
        user = IMUser.get_user(db, id=user_id)
        await ws_ctrl.send_friend_msg(user_id, friend_id, msg.id, user.nickname, user.avatar,
                                      datetime_format(sent_time))
        return True

    async def agree_friend(self, db, msg_id, user_id):
        msg = IMAddFriendMessage.get(db, id=msg_id)
        if not msg:
            raise HTTPException(status_code=400, detail=render_error("好友申请已同意"))
        if msg.to_user_id != user_id:
            raise HTTPException(status_code=400, detail=render_error("这条申请不属于当前用户"))
        if msg.is_agree:
            raise HTTPException(status_code=400, detail=render_error("好友申请已同意"))
        cur_user_contact = IMUserContact.get_user_contact(db, user_id)
        if not cur_user_contact:
            IMUserContact.create(db, user_id=user_id, friends=[msg.from_user_id])
        else:
            if msg.from_user_id not in cur_user_contact.friends:
                friends = list(cur_user_contact.friends)
                friends.append(msg.from_user_id)
                cur_user_contact.friends = friends
            else:
                raise HTTPException(status_code=400, detail=render_error("该好友已经添加过了"))
        friend_contact = IMUserContact.get_user_contact(db, msg.from_user_id)
        if not friend_contact:
            IMUserContact.create(db, user_id=msg.from_user_id, friends=[user_id])
        else:
            if user_id not in friend_contact.friends:
                friends = list(friend_contact.friends)
                friends.append(user_id)
                friend_contact.friends = friends
            else:
                raise HTTPException(status_code=400, detail=render_error("该好友已经添加过了"))
        msg.is_agree = True
        db.commit()
        await ws_ctrl.friend_agree_msg(msg.from_user_id)

    def get_friend_msg(self, db, user_id):
        return db.query(IMAddFriendMessage.id, IMAddFriendMessage.sent_time, IMUser.nickname, IMUser.avatar).filter(
            IMAddFriendMessage.to_user_id == user_id,
            IMAddFriendMessage.is_agree == False
        ).join(IMUser, IMUser.id == IMAddFriendMessage.from_user_id).all()

    def delete_contact(self, db, user_id, friend_id):
        user_friend = IMUserContact.get_user_contact(db, user_id)
        if not user_friend or friend_id not in user_friend.friends:
            raise HTTPException(status_code=400, detail=render_error(f'该用户找不到id为{friend_id}的联系人'))
        user_friend.friends.remove(friend_id)
        return True

    def get_user(self, db, **kwargs):
        return db.query(IMUser).filter_by(**kwargs).first()

    def get_contacts(self, db, user_id):
        user_contact: IMUserContact = IMUserContact.get_user_contact(db, user_id)
        if not user_contact:
            return None
        return db.query(IMUser).filter(IMUser.id.in_(user_contact.friends)).all()

    def register(self, db: Session, **kwargs):
        if self.get_user(db, account=kwargs['account']):
            raise HTTPException(status_code=400,
                                detail=render_resp(message=f"account {kwargs['account']} already in use."))
        passwd = hashlib.md5(kwargs['passwd'].encode()).hexdigest()
        # passwd = get_password_hash(kwargs['passwd'])
        kwargs['passwd'] = passwd
        user = IMUser(**kwargs)
        db.add(user)
        db.commit()
        db.refresh(user)
        return user

    async def create_token(self, db: Session, account: str, passwd: str):
        passwd = hashlib.md5(passwd.encode()).hexdigest()
        # passwd = get_password_hash(passwd)
        user: IMUser = db.query(IMUser).filter(or_(IMUser.account == account, IMUser.email == account),
                                               IMUser.passwd == passwd).first()
        if not user:
            raise HTTPException(status_code=400, detail=render_resp(message=f"Incorrect account or passwd."))

        user_data = dict(id=user.id)
        access_token = await create_access_token(user_data, settings.ACCESS_TOKEN_EXPIRE_SECONDS)
        return dict(access_token=access_token, expire_in=settings.ACCESS_TOKEN_EXPIRE_SECONDS)


user_ctrl = UserController()
