from pydantic import BaseModel, Field, validator
from datetime import datetime, date

from src.utils import datetime_format


class UserScheme(BaseModel):
    nickname: str
    account: str
    passwd: str
    email: str = None
    avatar: str = None
    gender: str = 0


class UserModelScheme(BaseModel):
    id: str
    uid: str
    username: str
    account: str
    nickname: str
    email: str = None
    avatar: str = None
    gender: str = 0
    create_time: datetime
    update_time: datetime

    # 向 Pydantic 提供配置
    class Config:
        #  orm_mode 会告诉 Pydantic 模型读取数据，即使它不是字典，而是 ORM 模型（或任何其他具有属性的任意对象）
        orm_mode = True

        json_encoders = {
            # custom output conversion for datetime
            datetime: datetime_format
        }
