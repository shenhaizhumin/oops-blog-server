from datetime import datetime
import uuid

from sqlalchemy import String, Integer, Column, DateTime, SmallInteger, JSON, Boolean

from src.core.db.db_session import Base, engine
from src.utils import get_random_username


class IMUser(Base):
    __tablename__ = "im_user"
    id = Column(Integer, primary_key=True)
    uid = Column(String, nullable=False, index=True, default=str(uuid.uuid4()).replace('-', ''))
    account = Column(String, nullable=False, comment="用户账号")
    passwd = Column(String, nullable=False, comment="用户密码")
    username = Column(String, default=get_random_username(), nullable=False, comment="用户名称")
    nickname = Column(String, nullable=False, comment="用户昵称")
    avatar = Column(String, comment="用户头像")
    email = Column(String)
    mobile = Column(String)
    gender = Column(SmallInteger)
    create_time = Column('create_time', DateTime, default=datetime.now)
    update_time = Column('update_time', DateTime, default=datetime.now, onupdate=datetime.now)

    @classmethod
    def get_user(cls, db, **kwargs):
        return db.query(cls).filter_by(**kwargs).first()


class IMUserContact(Base):
    __tablename__ = "im_user_contact"
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, nullable=False)
    friends = Column(JSON, nullable=False)
    create_time = Column('create_time', DateTime, default=datetime.now)

    @classmethod
    def get_user_contact(cls, db, user_id):
        return db.query(cls).filter_by(user_id=user_id).first()

    @classmethod
    def create(cls, db, **kwargs):
        c = cls(**kwargs)
        db.add(c)
        # db.commit()


class IMAddFriendMessage(Base):
    __tablename__ = 'im_add_friend_message'
    id = Column(Integer, primary_key=True)
    from_user_id = Column(Integer, nullable=False)
    to_user_id = Column(Integer, nullable=False)
    sent_time = Column(DateTime, default=datetime.now)
    is_agree = Column(Boolean, default=False)

    @classmethod
    def create(cls, db, **kwargs):
        msg = cls(**kwargs)
        db.add(msg)
        db.commit()
        db.refresh(msg)
        return msg

    @classmethod
    def get(cls, db, **kwargs):
        return db.query(cls).filter_by(**kwargs).first()


# Base.metadata.drop_all(bind=engine)
Base.metadata.create_all(bind=engine)
