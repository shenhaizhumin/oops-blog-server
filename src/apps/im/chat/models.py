from datetime import datetime
import uuid

from sqlalchemy import String, Integer, Column, DateTime, SmallInteger, ForeignKey, or_
from sqlalchemy.orm import relationship

from src.core.db.db_session import Base, engine
from .. import MessageType


class IMChat(Base):
    __tablename__ = "im_chat"

    id = Column(Integer, primary_key=True)
    uid = Column(String, nullable=False, default=str(uuid.uuid4()), comment="该条消息所属消息")
    chat_uid = Column(String, nullable=False, index=True, comment="服务器生产会话id")

    from_user_id = Column(Integer, nullable=False, comment="发送人id")
    to_user_id = Column(Integer, nullable=False, comment="接收人id")
    last_msg = Column(String, comment="最后的一条消息内容")
    last_msg_id = Column(Integer, comment="最后的一条消息id")
    last_user_nickname = Column(String, nullable=False, comment="最后的发送者名称")
    create_time = Column('create_time', DateTime, default=datetime.now)
    last_time = Column('last_time', DateTime, default=datetime.now, onupdate=datetime.now)
    chat_type = Column(SmallInteger, comment="会话类型(群组消息/个人聊天/系统消息)")
    msg_type = Column(SmallInteger, comment="消息类型(文字/图片/文件/音乐等)")
    unread_count = Column(Integer, comment="该会话未读数目")

    # user_chat_relation_id = Column(Integer, comment="用户关联会话的id")

    # backref 添加一个反向查询的属性
    # message_list = relationship('IMMessage', backref='message_list', uselist=True)

    @classmethod
    def get_info(cls, db, **kwargs):
        return db.query(cls).filter_by(**kwargs).first()

    @classmethod
    def create(cls, db, **kwargs):
        chat = cls(**kwargs)
        db.add(chat)
        db.commit()
        # db.refresh(chat)
        # return chat


class IMMessage(Base):
    __tablename__ = "im_message"
    """
    字段    字段类型    字段描述    备注
M_ID    Int    (消息ID)    主键，自增
M_PostMessages    Text    (消息内容)    
M_ status    Bit    (接收状态)    
M_Time    Datetime    (发送时间)    默认值
M_MessagesTypeID    Int    (消息类型ID)    外键
M_ FromUserID     Int    (发送者ID)指向用户表    外键
M_ToUserID     Int    (接收者ID)指向用户表    外键
    """
    id = Column(Integer, primary_key=True)
    message = Column(String, nullable=False, comment="消息内容")
    status = Column(SmallInteger, comment="接收状态")
    sent_time = Column(DateTime, comment="发送时间")
    message_type = Column(SmallInteger, default=MessageType.TEXT, comment="消息类型")
    # chat_uid = Column(String, ForeignKey('im_chat.chat_uid'), comment="会话id")
    chat_uid = Column(String, comment="会话id")
    from_user_id = Column(Integer, comment="发送人id")
    to_user_id = Column(Integer, comment="接收人用户id")

    @classmethod
    def create_message(cls, db, **kwargs):
        msg = cls(**kwargs)
        db.add(msg)
        db.commit()
        db.refresh(msg)
        return msg

    """
    id->auto increament primary key 自增长主键
 
uid->integer/varchar 该条消息所属消息，比如我登陆了，我发送/接收到消息入库的时候写入自己的uid，他的作用是多用户登陆的时候区分回话表
 
chatId->integet/varchar 服务器生产回话 id当前的回话id，它作用是标识一个回话，比如我跟你聊天or 你跟我聊天，我们的回话id应该是一致的，对于群聊也是，在群中发送消息，每个人的回话id是一致的。
 
c_id->varchar unique，他是标记一台设备上某个用户的唯一回话，用于更新避免插入多条数据的，可以用到sqlite的update or replace，他的值可以是hash(uid+chatid)或者其他，该字段可以只是客户端具有，客户端生成并且自己维护。
 
from->integer/varchar 发送人id(自己发送就是自己的uid，不然就是别人的uid)
 
to->integer/varchar 接收人id(uid/group_id)
 
last_msg->varchar 最后的一条消息内容
last_msg_id->integer/varchar  最后一条消息的id，作用是用于比较，比如在聊天页面中，删除了一条消息，撤回了一条消息之类的，这时候可以根据这个msg_id进行删除修改和更新。
chat_name -> varchar 聊天者的名称，比如我与你聊天，我的表中的这个字段就是你的name,你的表中就是我的name.
 
last_user_name->varchar 最后的发送者名称
 
last_time->integer 最后消息发送时间
 
chat_type->integer 回话类型(群组消息/个人聊天/系统消息)
 
msg_type->integer 消息类型(文字/图片/文件/音乐等)
 
unread_count->integer 改回话未读数目
    """


"""

 
id->integer auto increament primary key 自增长主键
 
msg_id 消息唯一id，一般服务器生成，或者客户端本地使用UUID生成
 
uid->Interger/varchar 所属者uid
 
is_me->Integer 是否是自己发送的(UI显示区分)
 
from->Interger/varchar  发送者uid(可以是自己)
 
from_avatar->Interger/varchar 发送者头像
 
from_name-> varchar 发送者名称
 
to-> Interger/varchar 接受者(uid/group_id)
 
chat_type 会话类型
 
msg_type 消息类型
 
msg-> 消息内容
 
file_info->文件信息json格式
 
send_time->发送时间
 
send_status->发送状态 发送中，发送完成，发送失败
 
extra->把人插入 一般可以为null，预留的额外字段，使用JOSN字符串存储
 
复制代码
"""

# class IMChatDetail(Base):
#     __tablename__ = "im_chat_detail"
#     id = Column(Integer, primary_key=True)
#     uid = Column(String, nullable=False, default=str(uuid.uuid4()))

Base.metadata.create_all(bind=engine)
