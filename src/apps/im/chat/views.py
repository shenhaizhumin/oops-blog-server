import json
import hashlib

from fastapi import APIRouter, Depends, Path
from fastapi.websockets import WebSocket, WebSocketDisconnect

from .controllers import ws_ctrl
from src.core.jwt_token import get_current_user, get_user_by_token
from src.core.response.response import render_error, render_resp, render_list_resp
from src.core.db.db_session import get_db
from . import schemes

ws_router = APIRouter()


@ws_router.get("/getChatMessageList/{chatUid}", description="获取会话消息列表")
async def get_message_list(
        chat_uid: str = Path(..., alias="chatUid", description="会话uid"),
        db=Depends(get_db)
):
    messages = ws_ctrl.get_message_list(db, chat_uid)
    return render_list_resp([schemes.IMMessageModel.from_orm(m) for m in messages])


@ws_router.get("/getChats", description="获取用户所有会话")
async def get_chats(
        user=Depends(get_current_user),
        db=Depends(get_db)
):
    return render_list_resp(ws_ctrl.get_chats(db, user['id']))


@ws_router.post("/create_chat/{to_user_id}", description="建立会话")
async def create_chat(
        to_user_id: int = Path(...),
        user=Depends(get_current_user),
        db=Depends(get_db)
):
    return render_resp(ws_ctrl.create_chat(db, to_user_id, user['id'], user['nickname']))


@ws_router.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket, db=Depends(get_db)):
    token = websocket.headers.get("Authentication")
    print(f"token:{token}")
    user = await get_user_by_token(db, token)
    await ws_ctrl.connect(user['id'], websocket)

    # await ws_ctrl.broadcast(f"用户{user}进入聊天室")

    try:
        while True:
            data = await websocket.receive_text()
            # print(f"data:{data}")
            print(f"收到消息：userid:{user['id']}  {data}")
            if data == 'heartbeat':
                continue
            try:
                params = json.loads(data, encoding='utf8')
                params.update(nickname=user['nickname'])
            except:
                await ws_ctrl.clients.get(user['id']).send_text(
                    ws_ctrl.create_message(render_error('仅支持json格式的参数'), -1)
                )
                continue
            await ws_ctrl.send_msg(db, user['id'], **params)
            # await ws_ctrl.send_personal_message(f"你说了: {data}", websocket)
            # await ws_ctrl.broadcast(f"用户:{user} 说: {data}")

    except WebSocketDisconnect:
        ws_ctrl.disconnect(user['id'])
        # await ws_ctrl.broadcast(f"用户-{user}-离开")
