from typing import List, Dict
from datetime import datetime
import json
import hashlib

from sqlalchemy import or_, and_
from sqlalchemy.orm import Session
from starlette.websockets import WebSocket, WebSocketState

from .models import IMMessage, IMChat
from .. import MessageStatus, MessageType, SessionType
from src.utils import time_mills_to_datetime
from src.core.response.response import render_error, render_resp
from ..user.models import IMUser


class MsgType:
    NORMAL = -1
    SESSION = 0
    FRIEND = 1


class WsController:
    def __init__(self):
        # 存放激活的ws连接对象
        self.clients: Dict[int, WebSocket] = dict()

    def create_message(self, data: dict, type: int = MsgType.SESSION):
        data.update(type=type)
        return json.dumps(data, ensure_ascii=False)

    def get_message_list(self, db, chat_uid):
        return db.query(IMMessage).filter(
            IMMessage.chat_uid == chat_uid
        ).order_by(IMMessage.id.asc()).all()

    def get_chats(self, db: Session, user_id):
        return db.query(
            IMChat, IMUser.nickname, IMUser.avatar, IMUser.id.label("friend_user_id")
        ).filter(
            or_(IMChat.from_user_id == user_id, IMChat.to_user_id == user_id)
        ).join(
            IMUser,
            or_(and_(IMChat.from_user_id != user_id, IMUser.id == IMChat.from_user_id),
                and_(IMChat.to_user_id != user_id, IMUser.id == IMChat.to_user_id))
            # or_(IMUser.id == IMChat.from_user_id, IMUser.id == IMChat.to_user_id)
        ).all()

    def create_chat(self, db, to_user_id, user_id, nickname):
        uid1 = hashlib.md5(f'{user_id}_{to_user_id}'.encode()).hexdigest()
        uid2 = hashlib.md5(f'{to_user_id}_{user_id}'.encode()).hexdigest()
        chat = db.query(IMChat).filter(IMChat.chat_uid.in_([uid1, uid2])).first()
        if not chat:
            self.create_chat_session(db, uid1, to_user_id, user_id, nickname)
            return uid1
        else:
            return chat.chat_uid

    def create_chat_session(self, db, uid, to_user_id, user_id, nickname):
        IMChat.create(
            db,
            from_user_id=user_id,
            to_user_id=to_user_id,
            last_user_nickname=nickname,
            chat_uid=uid,
            # last_time=last_sent_time,
            # last_msg=last_msg,
            # last_msg_id=last_msg_id,
            chat_type=SessionType.ONE_TO_ONE,
            # msg_type=message_typ
        )

    async def send_msg(self, db, user_id, **params):
        """
        {
            "to_user_id":2,"message":"你好","sent_time":1662205773238,"chat_uid":"ab35e84a215f0f711ed629c2abb9efa0"
        }
        """
        if not all([params.get('to_user_id'), params.get('message'), params.get('sent_time'), params.get('chat_uid')]):
            await self.clients.get(user_id).send_text(
                self.create_message(render_error("参数不正确to_user_id/message/sent_time"), MsgType.NORMAL))
            return

        nickname = params['nickname']
        from_user_id = user_id
        to_user_id = params['to_user_id']
        chat_uid = params['chat_uid']
        message = params['message']
        sent_time = params['sent_time']
        from_client = self.clients.get(user_id)
        uid1 = hashlib.md5(f'{user_id}_{to_user_id}'.encode()).hexdigest()
        uid2 = hashlib.md5(f'{to_user_id}_{user_id}'.encode()).hexdigest()
        if chat_uid not in [uid1, uid2]:
            await from_client.send_text(self.create_message(render_error("会话id有误"), MsgType.NORMAL))
            return
        last_sent_time = time_mills_to_datetime(sent_time)
        message_type = MessageType.TEXT
        msg = IMMessage.create_message(
            db,
            from_user_id=from_user_id,
            chat_uid=chat_uid,
            to_user_id=to_user_id,
            message_type=message_type,
            message=message,
            sent_time=last_sent_time
        )
        chat = IMChat.get_info(db, chat_uid=chat_uid)
        last_msg = message
        last_msg_id = msg.id
        last_user_nickname = nickname

        if not chat:
            print(f"chat not found: {chat_uid}")
            if from_client:
                await from_client.send_text(self.create_message(render_error("找不到对应的会话"), MsgType.NORMAL))
            return

        else:
            chat.last_msg = last_msg
            chat.last_time = last_sent_time
            chat.last_user_nickname = last_user_nickname
            chat.from_user_id = from_user_id
            chat.to_user_id = to_user_id
            chat.chat_type = SessionType.ONE_TO_ONE
            chat.msg_type = message_type
            chat.last_msg_id = last_msg_id
            db.commit()
        to_client = self.clients.get(to_user_id)
        if to_client:
            await to_client.send_text(
                self.create_message(
                    render_resp(dict(form_user_id=from_user_id,
                                     to_user_id=to_user_id,
                                     message_type=message_type,
                                     chat_uid=chat_uid,
                                     message=message,
                                     sent_time=last_sent_time)
                                ), MsgType.SESSION)
            )
            if from_client:
                await from_client.send_text(self.create_message(dict(message='发送成功'), MsgType.NORMAL))

    async def connect(self, user_id: int, ws: WebSocket):
        # 等待连接
        await ws.accept()
        # 存储ws连接对象
        self.clients[user_id] = ws

    def disconnect(self, user_id: int):
        # 关闭时 移除ws对象
        if user_id in self.clients.keys():
            self.clients.pop(user_id)

    @staticmethod
    async def send_personal_message(message: str, ws: WebSocket):
        # 发送个人消息
        await ws.send_text(message)

    async def broadcast(self, message: str):
        # 广播消息
        for connection in self.clients.values():
            if self.check_websocket_state(connection):
                await connection.send_text(message)

    async def check_websocket_state(self, ws: WebSocket):
        return ws.client_state == WebSocketState.CONNECTED

    async def friend_agree_msg(self, friend_id):
        friend_client = self.clients.get(friend_id)
        print(f"你已同意申请，发消息给申请人:{friend_client}")
        if friend_client:
            await friend_client.send_text(self.create_message(render_resp(dict(is_agree=True)), MsgType.FRIEND))

    async def send_friend_msg(self, user_id, friend_id, msg_id, nickname, avatar, sent_time):
        friend_client = self.clients.get(friend_id)
        cur_client = self.clients.get(user_id)
        if cur_client:
            await cur_client.send_text(self.create_message(render_error("好友申请已发送"), MsgType.NORMAL))
        if friend_client:
            await friend_client.send_text(self.create_message(
                render_resp(dict(id=msg_id, nickname=nickname, avatar=avatar, sent_time=sent_time)), MsgType.FRIEND))


ws_ctrl = WsController()
