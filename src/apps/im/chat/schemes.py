from datetime import datetime

from pydantic import BaseModel

from src.utils import datetime_format


class IMMessageModel(BaseModel):
    id: str
    message: str
    # status: int = 0
    sent_time: datetime
    message_type: int
    chat_uid: str
    from_user_id: int
    to_user_id: int

    class Config:
        #  orm_mode 会告诉 Pydantic 模型读取数据，即使它不是字典，而是 ORM 模型（或任何其他具有属性的任意对象）
        orm_mode = True

        json_encoders = {
            # custom output conversion for datetime
            datetime: datetime_format
        }
