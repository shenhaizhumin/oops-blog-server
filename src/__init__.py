"""

# 加载人名识别镜像
docker load -i lac-server.tar

# 镜像名称：lac-server，镜像tag：V1.0
# 启动服务 port：容器内服务映射到宿主机器的端口号
docker run -tid --name lac-server -p ${port}:8000 lac-server:V1.0

"""